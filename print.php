<?php
  include "lib/spyc.php";
  include "lib/Parsedown.php";
  include "functions.php";
  $spyc = new Spyc();
  $p =  new Parsedown();
?>
<!DOCTYPE html>
<html>
<head>
  <title>aDaad PRINT</title>
  <link rel="stylesheet" href="css/preprint.css">
  <link rel="stylesheet" media="print" href="print.css">
</head>
  <body>
    <?php
      $directory = "projects";
      $projects = rglob($directory."/{*.md}", GLOB_BRACE);
      foreach ($projects as $project) {
        $y = Spyc::YAMLLoad($project);
        $project_dir = dirname($project);
        $project_slug = basename($project_dir);
        $publish = isset($_POST[$project_slug]);
        $title = $y['title'];
        $text_fr = $y['text_fr'];
        $text_en = $y['text_en'];
        echo "<section class='project'>";
        echo "<h1>$title</h1>";
        echo "<div class='fr' name='text_fr'>$text_fr</div>";
        echo "<div class='en' name='text_en'>$text_en</div>";
        echo "</section>";
      }
    ?>
    <script src="js/paged.legacy.polyfill.js" charset="utf-8"></script>
    <script src="js/main.js" charset="utf-8"></script>
  </body>
</html>
