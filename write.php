<?php
include "lib/spyc.php";
$spyc = new Spyc();
$project_dir = $_POST["project_dir"];
$title   = $_POST["title"];
$text_fr = $_POST["text_fr"];
$text_en = $_POST["text_en"];

$project_file = $project_dir."/README.md";
$y = Spyc::YAMLLoad($project_file);
$y['title']   = $title;
$y['text_fr'] = $text_fr;
$y['text_en'] = $text_en;
$output = Spyc::YAMLDump($y);

$fh = fopen($project_file, 'w') or die("can't open file");
fwrite($fh, $output);
fclose($fh);
?>
