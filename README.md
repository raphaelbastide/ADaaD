# ADAAD

Another Directory as a database (PHP, `XMLHttpRequest` and json)

![screenshot](adaad-screenshot.gif)

This project is super simple and can be highly improved. Do not use in production.

Uses [paged.js](https://gitlab.pagedmedia.org/tools/pagedjs) to generate a print version of the content.
