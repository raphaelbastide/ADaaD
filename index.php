<?php
  include "lib/spyc.php";
  include "lib/Parsedown.php";
  include "functions.php";
  $spyc = new Spyc();
  $p =  new Parsedown();
?>
<!DOCTYPE html>
<html>
<head>
  <title>aDaad DEMO</title>
  <link rel="stylesheet" href="css/main.css">
</head>
  <body>
      <header>
        <a href="print.php">Print</a>
      </header>
    <?php
      $directory = "projects";
      $projects = rglob($directory."/{*.md}", GLOB_BRACE);
      foreach ($projects as $project) {
        $y = Spyc::YAMLLoad($project);
        $project_dir = dirname($project);
        $project_slug = basename($project_dir);
        $publish = isset($_POST[$project_slug]);
        $title = $y['title'];
        $text_fr = $y['text_fr'];
        $text_en = $y['text_en'];
        echo "<section>";
        echo "<form id='form-$project_slug' method='POST' action='write.php' onsubmit='return submitForm(this);' >";
        echo "<input hidden type='text' name='project_dir' value='$project_dir'/>";
        echo "<input class='col' type='text' name='title' value='$title'/>";
        echo "<textarea class='fr col' name='text_fr'>$text_fr</textarea>";
        echo "<textarea class='en col' name='text_en'>$text_en</textarea>";
        echo "<input class='col' type='submit' value='Update'/>";
        echo "</form>";
        echo "</section>";
      }
    ?>
    <script src="js/main.js" charset="utf-8"></script>
  </body>
</html>
